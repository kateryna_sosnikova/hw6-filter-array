let array = ['hello', 'world', 23, '23', null, {a: name, b: 150}, true];

function filterBy(array, type) {

    return array.filter(item => {
        if (type === null && item === null) return false;
        if (type === 'object' && item === null) return true;
        return typeof item !== type;
    })
}
console.log(filterBy(array, 'object'));